<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\Middleware\ErrorBinder;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function postSignup(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'name' =>'required|max:120',
            'password' => 'required|min:4'
        ]);

        $email = $request['email'];
        $name = $request['name'];
        $password = bcrypt($request['password']);

        $user = new User();
        $user->email = $email;
        $user->name = $name;
        $user->password = $password ;

        $user->save();
        Auth::login($user);
        return redirect()->route('admin');
    }
    public function postSignin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);
        if(Auth::attempt(['email' => $request['email'], 'password' => $request['password']])){
            return redirect()->route('dashboard');
        }
        return redirect()->back();
    }

    public function getAdmin()
    {
        return view('admin.admin');
    }

    public function getLogout()
    {
        Auth::logout();
        return redirect()->route('admin');
    }
}
