<?php

namespace App\Http\Controllers;
use App\Http\Requests;
use Carbon\Carbon;
use Guzzle;
use App\sliderImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImagController extends Controller
{

    public function upload(){
        return view('admin.imageupload');
    }

    /**
     * Store a newly uploaded resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $image = new sliderImage();
        $this->validate($request, [
            'titre' => 'required',
            'image' => 'required'
        ]);
        $image->titre = $request->titre;
        $image->description = $request->description;
        if($request->hasFile('image')) {
            $file = Input::file('image');
            //getting timestamp
            $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());

            $name = $timestamp. '-' .$file->getClientOriginalName();

            $image->filePath = $name;

            $file->move(public_path().'/images/', $name);
        }
        $image->save();
        return $this->upload()->with('success', 'Image Uploaded Successfully');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function show(Request $request)
    {
        $images = sliderImage::all();
        return view('admin.showLists', compact('images'));

    }
}
