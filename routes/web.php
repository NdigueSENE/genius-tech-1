<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/acceuil',[
    'uses' =>'Controller@getAcceuil',
    'as' =>'acceuil'
]);

Route::get('/admin',[
    'uses' =>'UserController@getAdmin',
    'as' =>'admin'
]);

Route::post('/signup',[
    'uses' => 'UserController@postSignup',
    'as' => 'signup'
]);

Route::post('/signin',[
    'uses' => 'UserController@postSignin',
    'as' => 'signin'
]);

Route::get('/logout',[
    'uses' =>'UserController@getLogout',
    'as' =>'logout'
]);

Route::post('/logout',[
    'uses' =>'UserController@getLogout',
    'as' =>'logout'
]);

Route::get('dashboard', [

    'uses' => 'PostController@getDashboard',
    'as' => 'dashboard',
    'middleware' => 'auth'

]);

Route::post('/envoiMessage',[
    'uses' => 'PostController@postMessage',
    'as' => 'envoiMessage'
]);

Route::post('/sendmail', function(\Illuminate\Http\Request $request, \Illuminate\Mail\Mailer $mailer){
	$mailer->to ('geniustechsn@gmail.com')
        ->send(new \App\Mail\webmail ($request->input('title'),$request->input('email') ));
    return redirect()->back();
    })->name('sendmail');

Route::get('imageUploade', 'ImagController@upload' );

Route::post('/imageUploadForm', [
    'uses' => 'ImagController@store',
    'as' => 'imageUploadForm' ]
);

Route::get('showLists', 'ImagController@show' );