<section id="contact">
    <div id="google-map"  class="wow fadeIn" data-latitude="14.682123" data-longitude="-17.466716" data-wow-duration="1000ms" data-wow-delay="400ms">

    </div>
    <div id="contact-us" class="parallax">
        <div class="container">
            <div class="row">
                <div class="heading text-center col-sm-8 col-sm-offset-2 wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <h2>Contact : </h2>
                    <p></p>
                </div>
            </div>
            <div class="contact-form wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
                <div class="row">
                    <div class="col-sm-6">
                        <form  method="post" action="{{ route('sendmail') }}">

                            <div class="row  wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" name="nom" id="nom" class="form-control" placeholder="Nom complet" required="required">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Adresse email" required="required">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" name="sujet" id="sujet" class="form-control" placeholder="Sujet" required="required">
                            </div>
                            <div class="form-group">
                                <textarea name="title" id="title" class="form-control" rows="4" placeholder="Ecrivez vos messages" required="required"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn-submit">Envoyer</button>
                                {{ csrf_field() }}
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <div class="contact-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation.</p>-->
                            <ul class="address">
                                <li><i class="fa fa-map-marker"></i> <span> Adresse:</span> Dakar </li>
                                <li><i class="fa fa-phone"></i> <span> T�l�phone:</span> +221 77 674 66 49   </li>
                                <li><i class="fa fa-envelope"></i> <span> Email:</span><a href="mailto:someone@yoursite.com"> support@geniustech.sn</a></li>
                                <li><i class="fa fa-globe"></i> <span> Site web:</span> <a href="http://www.geniustechsoft.com">www.geniustechsoft.com</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
