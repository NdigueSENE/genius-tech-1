
<section id="about-us" class="parallax">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="about-info wow fadeInUp" data-wow-duration="1000ms" data-wow-delay="300ms">
                    <h1 style="color: #2e6da4"><span>Genius Tech</span></h1>
                    <p>Société de services en ingénierie informatique (SSII) implantée à Dakar. Nous sommes spécialisé dans le développement d'applications sur mesures (ERP, BI, GPAO...),
                        . </p>
                    <p>  la création de sites internet/intranet, vitrine, e-commerce, B2B,
                        le référencement et positionnement de site internet,
                        l'infogérance de serveur web,
                        la gestion, les audits et la protection de parcs informatiques,
                        l'installation et le paramétrage de serveurs (Linux/Windows).</p>
                </div>
            </div>
            <div class="col-sm-6">
                <img src="images/logo-genius.png" class="img-responsive">
            </div>
        </div>
    </div>
</section><!--/#about-us-->
<section id="features" class="parallax">
    <div class="container">
        <div class="row count">
            <div class="col-sm-12 col-xs-12 wow fadeInLeft" data-wow-duration="1000ms" data-wow-delay="300ms">
                <span class="fa fa-check-circle fa-5x"></span>
                <br><br>
                <span class="timer" style="font-size: 408%;">100</span> <span style="font-size: 408%;">%</span>
                <br><br>

                <p style="font-size: 608%;">Garantie</p>
            </div>
        </div>
    </div>
</section>
<!--/#features-->